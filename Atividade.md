# Atividade

O atividade consiste de três etapas:

* Construir um componente que permita exibir um ícone em específico, quantidade e título de um determinado item.

  ![1](/images/1.png)

* Adicionar a capacidade de listagem do items, a partir do componente anterior.
  ![2](/images/2.png)

* A partir do click em um dos elementos da tabela, acionar uma modal, que contenha um formulário preenchido com os dados do elementos clicados e permite salvar os dados deste formulário a partir de uma requisição PUT
  ![3](/images/3.png)

# Info Adicional
Deve-se cria um fork deste projeto, e após a conclusão, no enviar o link do repositório com o fork

Qualquer dúvida adicional, sobre a atividade, entre em contato

<danilo.santos@geraeb.con.br>


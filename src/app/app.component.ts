import { Component } from '@angular/core';
import { Distribuidora, Empresa } from './shared/models/index';
import { ServerCommunicationService } from './services/server-communication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  distribuidoras: [Distribuidora] = null;
  distribuidorasIcon: string;
  empresas: [Empresa] = null;
  empresasIcon: string;
  isLoaded: boolean;

  constructor(private server: ServerCommunicationService) {
    this.server.getDistribuidoras().subscribe((distribuidoras: [Distribuidora]) => {
      this.distribuidorasIcon = "industry";
      this.distribuidoras = distribuidoras;

      if (this.empresas != null && this.distribuidoras != null) {
        this.isLoaded = true;
      }
    });

    this.server.getEmpresas().subscribe((empresas: [Empresa]) => {
      this.empresasIcon = "building";
      this.empresas = empresas;

      if (this.empresas != null && this.distribuidoras != null) {
        this.isLoaded = true;
      }
    });
  }
}

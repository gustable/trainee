import { Component, OnInit, Input, Output } from '@angular/core';
import { Distribuidora, Empresa } from '../../shared/models/index';

@Component({
  selector: 'trainee-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() icon: string;
  @Input() company: [Distribuidora] | [Empresa];

  quantity: number;
  title: string;
  showTable: boolean;

  constructor() { }

  ngOnInit() {
    this.quantity = this.company.length;

    if (this.quantity > 0) {
      this.title = this.getTitle(this.company[0]);
    }

    this.showTable = false;
  }

  toggleList(): void {
    this.showTable = !this.showTable;
    // let container = document.querySelector(".component-container");
    // if (this.showTable) {
    //   (container as HTMLElement).style.cssText = "--height: 150px";
    // } else {
    //   (container as HTMLElement).style.cssText = "--height: 100px";
    // }
  }

  getTitle(company: Distribuidora | Empresa): string {
    if (typeof (company['nome_fantasia']) === 'undefined') {
      return "Distribuidoras";
    } else {
      return "Empresas";
    }
  }

}

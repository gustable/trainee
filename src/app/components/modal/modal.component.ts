import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Distribuidora, Empresa } from '../../shared/models/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServerCommunicationService } from '../../services/server-communication.service';

@Component({
  selector: 'trainee-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() rowData;
  @Output() closeModal: EventEmitter<boolean> = new EventEmitter();

  distribuidora: Distribuidora = null;
  empresa: Empresa = null;
  estados: string[];
  isActive: boolean;

  constructor(private server: ServerCommunicationService,
    private http: HttpClient) { }

  ngOnInit() {
    this.estados = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"];
    this.isDistribuidora(this.rowData);
  }

  isDistribuidora(rowData): void {
    if (typeof (rowData['nome_fantasia']) === 'undefined') {
      this.distribuidora = rowData;
    } else {
      this.empresa = rowData;
    }
  }

  close() {
    this.closeModal.emit(true);
  }

  distribuidoraSubmit() {
    this.server.updateDistribuidora(this.distribuidora).subscribe(() => {
      console.log("Distribuidora updated");
      this.close();
    });
  }

  empresaSubmit() {
    console.log("submitted");
    this.server.updateEmpresa(this.empresa).subscribe(() => {
      console.log("Empresa updated");
      this.close();
    });
  }
}

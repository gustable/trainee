import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Distribuidora, Empresa } from '../../shared/models/index';
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'trainee-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() companies: [Empresa] & [Distribuidora];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource;
  displayedColumns: string[];
  showModal: boolean = false;
  currentRowClicked: Empresa | Distribuidora = null;

  constructor() { }

  ngOnInit() {
    this.setTableContent();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  isDistribuidora(companies): boolean {
    if (typeof (companies[0]['nome_fantasia']) === 'undefined') {
      let distribuidoras: [Distribuidora] = companies;
      this.dataSource = new MatTableDataSource<Distribuidora>(distribuidoras);
      return true;
    } else {
      let empresas: [Empresa] = companies;
      this.dataSource = new MatTableDataSource<Empresa>(empresas);
      return false;
    }
  }

  setTableContent(): void {
    if (this.isDistribuidora(this.companies)) {
      this.displayedColumns = ['checkbox', 'name', 'estado'];
    } else {
      this.displayedColumns = ['checkbox', 'cnpj', 'nome_fantasia'];
    }
  }

  openModal(row): void {
    this.showModal = true;
    this.currentRowClicked = row;
  }

  closeModal(closeModal): void {
    if (closeModal) {
      this.showModal = false;
    }
  }
}

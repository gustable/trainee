import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Distribuidora, Empresa } from '../shared/models/index';

@Injectable({
  providedIn: 'root'
})
export class ServerCommunicationService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getDistribuidoras(): Observable<[Distribuidora]> {
    return this.http.get<[Distribuidora]>('api/distribuidoras');
  }

  getEmpresas(): Observable<[Empresa]> {
    return this.http.get<[Empresa]>('api/empresas');
  }

  updateDistribuidora(distribuidora: Distribuidora): Observable<any> {
    return this.http.put(`api/distribuidoras/${distribuidora.id}`, distribuidora, this.httpOptions).pipe(
      tap(_ => console.log(`Distribuidora ${distribuidora.id} atualizada`))
    );
  }

  updateEmpresa(empresa: Empresa): Observable<any> {
    return this.http.put(`api/empresas/${empresa.id}`, empresa, this.httpOptions).pipe(
      tap(_ => console.log(`Empresa ${empresa.id} atualizada`))
    );
  }
}

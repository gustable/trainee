export class Empresa {
    id?: number | any;
    estado_id: {
        id: number;
        pais: {
            id: number;
            nome: string;
            sigla: string;
        },
        nome: string;
        uf: string;
    } | any;
    name: string | any;
    nome_fantasia: string | any;
    cnpj: string | any;
    endereco_faturamento: string | any;
    is_active: boolean | any;
    timestamp?: Date | any;
    created_by?: number | any;
}